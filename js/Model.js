/**
 * Model application
 * @constructor
 */
function Model() {
    /////////// CONSTANT /////////////
    /**
     * Api hostname prefix
     * @type {string}
     * @private
     */
    var _apiPrefix = "http://localhost:3000/api/";

    /**
     *  Url for orders
     * @type {string}
     * @private
     */
    var _orderURLTemplate = _apiPrefix + "Orders/";
    /**
     * Url for order search
     * @type {string}
     * @private
     */
    var _orderSearch = _apiPrefix + "Orders/?filter[where][or][0][summary.createdAt][regexp]={TEXT_VALUE}" +
                       "&filter[where][or][1][summary.customer][regexp]={TEXT_VALUE}" +
                       "&filter[where][or][2][summary.status][regexp]={TEXT_VALUE}" +
                       "&filter[where][or][3][summary.shippedAt][regexp]={TEXT_VALUE}" +
                       "&filter[where][or][4][summary.totalPrice][regexp]={TEXT_VALUE}" +
                       "&filter[where][or][5][summary.currency][regexp]={TEXT_VALUE}" +
                       "&filter[where][or][6][id]={TEXT_VALUE}";
    /**
     * Url with decode for map
     * @type {string}
     * @private
     */
    var _urlGeo = 'https://geocode-maps.yandex.ru/1.x/?apikey=e0bfaaae-7dc0-470c-8a0d-adcbd2177315&format=json&geocode=';

    /**
     *  Variable for storage orders
     * @type {Object[]}
     * @private
     */
    var _Orders = null;
    /**
     *  Variable for storage fields
     * @type {Object[]}
     * @private
     */
    var _infoFields = null;

    /**
     * Variable for storage products
     * @type {Object[]}
     * @private
     */
    var _products = null;

    /**
     * Save current order object
     * @type {Object[]}
     * @private
     */
    var _currentOrder = null;

    /////////// METHODS /////////////


    /**
     * Get list of orders from server
     * @returns {*|PromiseLike<T>|Promise<T>}
     */
    this.fetchOrders = function() {
        return this
            .fetchData(_orderURLTemplate)
            .then(function (ordersData) {
                _Orders = ordersData;
                return ordersData;
            })
            .catch(function (error) {
                alert(error.message);
            });
    };

    /**
     *  Get sort product from server
     * @param orderId {number | string}  - order id
     * @param sortvalue {number | string}  - sort field
     * @param typeSort {string} - asc | desc | default
     * @returns {*|PromiseLike<T>|Promise<T>}
     */
    this.fetchProductSort = function(orderId, sortvalue, typeSort) {
        return this
            .fetchOrderProduct(orderId, sortvalue, typeSort)
            .then(function (products) {
                _products = products;
                return products;
            })
            .catch(function (error) {
                alert(error.message);
            });
    };

    /**
     * Get orders by entered value
     * @param inputText {number | string}  - value from input
     * @returns {*|PromiseLike<T>|Promise<T>} - list of orders
     */
    this.fetchOrdersSearch = function(inputText) {
        if(inputText === undefined || inputText == null){
            return this.fetchData(_orderSearch.replace(new RegExp('{TEXT_VALUE}', 'g'), '.*') )
                .then(function (ordersData) {
                    _Orders = ordersData;
                    return ordersData;
                })
                .catch(function (error) {
                    alert(error.message);
                });
        } else {
            return this.fetchData(_orderSearch.replace(new RegExp('{TEXT_VALUE}', 'ig'), inputText) )
                .then(function (ordersData) {
                    _Orders = ordersData;
                    return ordersData;
                })
                .catch(function (error) {
                    alert(error.message);
                });
        }

    };


    /**
     * Set Current Order by Click.
     * If id is not transfer set first order active.
     * If there is NO first element set Current order = null
     * @param clickedId {number | string} - order id.
     */
    this.setCurrentOrder = function (clickedId) {
        if(clickedId == null || clickedId === undefined){
            if(_Orders[0]){
                _currentOrder = _Orders[0];
            } else {
                _currentOrder = null;
            }
        } else {
            _currentOrder =_Orders.find(function (value) {
                return value.id == clickedId;
            });
        }
    };

    /**
     * Save edited data and put on server
     * @param objectSave {Object} - object with uploaded info
     * @param currentId {number | string}  - current order id
     */
    this.fetchSave = function(objectSave, currentId) {
        var putOn;
        if(objectSave.objectsave.shipTo){
            putOn = objectSave.objectsave;
        } else if (objectSave.objectsave.customerInfo) {
            putOn = objectSave.objectsave;
        }
        var url = _orderURLTemplate + currentId;
        this.fetchDataPut(url, currentId, putOn);
    };

    /**
     * Fetch data for get request
     * @param url - {string}
     * @returns {Promise}
     */
    this.fetchData = function (url) {
        return new Promise(function (resolve, reject) {
            var req = new XMLHttpRequest();
            req.open("GET", url, true);
            req.addEventListener("load", function () {
                if (req.status < 400) {
                    resolve(JSON.parse(req.responseText));
                } else {
                    reject(new Error("Request failed: " + req.statusText));
                }
            });
            req.addEventListener("error", function () {
                reject(new Error("Network error"));
            });
            req.send();
        });
    };

    /**
     * Fetch data for put request
     * @param url {string}
     * @param currentId {number | string} - current order id
     * @param putOn {Object} - data which put on server
     * @returns {Promise} -
     */
    this.fetchDataPut = function (url, currentId, putOn) {
        return new Promise(function(succeed, fail) {
            var sendData = JSON.stringify(putOn);
            var request = new XMLHttpRequest();
            request.open("PUT", url, true);
            request.setRequestHeader('Content-Type', 'application/json');
            request.addEventListener("load", function() {
                if (request.status < 400){
                    succeed(request.responseText);
                }else{
                    fail(new Error("Request failed: " + request.statusText));
                }
            });
            request.addEventListener("error", function() {
                fail(new Error("Network error"));
            });
            request.send(sendData);
        });
    };

    /**
     *  Fetch data for post request
     * @param url {string}
     * @param postObject {Object} - object for post
     * @returns {Promise}
     */
    this.fetchDataPost = function (url, postObject) {
        return new Promise(function(succeed, fail) {
            var request = new XMLHttpRequest();
            request.open("POST", url, true);
            request.setRequestHeader('Content-Type', 'application/json');
            request.addEventListener("load", function() {
                if (request.status < 400)
                    succeed(request.responseText);
                else
                    fail(new Error("Request failed: " + request.statusText));
            });
            request.addEventListener("error", function() {
                fail(new Error("Network error"));
            });
            request.send(JSON.stringify(postObject));
        });
    };

    /**
     * Fetch for delete request
     * @param url {string}
     * @returns {Promise}
     */
    this.fetchDataDelete = function (url) {
        return new Promise(function(succeed, fail) {
            var request = new XMLHttpRequest();
            request.open("DELETE", url, true);
            request.setRequestHeader('Content-Type', 'application/json');
            request.addEventListener("load", function() {
                if (request.status < 400){
                    succeed(request.responseText);
                }else{
                    fail(new Error("Request failed: " + request.statusText));
                }
            });
            request.addEventListener("error", function() {
                fail(new Error("Network error"));
            });
            request.send();
        });
    };

    /**
     * Set Fields for form in modal
     * @param infoFields {number | string}
     */
    this.setorderFieldsInfo = function(infoFields) {
        _infoFields = infoFields;
    };

    /**
     * Get Fields for form in modal
     * @returns {Object[]}
     */
    this.getorderFieldsInfo = function() {
        return _infoFields;
    };


    /**
     * Get orders
     * @returns {Object[]}
     */
    this.getOrders = function () {
        return _Orders;
    };

    /**
     * Set orders in storage variable
     * @param orders
     */
    this.setOrders = function (orders) {
        _Orders = orders;
    };

    /**
     * Get current order
     * @returns {Object[]}
     */
    this.getCurrentOrder = function () {
        return _currentOrder;
    };

    /**
     * Get product from storage variable
     * @returns {Object[]}
     */
    this.getProducts = function () {
        return _products;
    };

    /**
     * Return image source from yandex map by coordinates
     * @param searchStr - {string} - place coordinates
     * @returns {Promise<string>}
     */
    this.getCodeMapYnadex = function (searchStr) {
        var url = _urlGeo + searchStr;
        return this
            .fetchData(url)
            .then(function (ordersData) {
                var coord = ordersData.response.GeoObjectCollection.featureMember[0].GeoObject.Point.pos;
                var neededFormatCoords = coord.replace(/ /g, ',');
                var imageSrc = 'https://static-maps.yandex.ru/1.x/?ll='+ neededFormatCoords +'&size=450,450&z=13&l=map&pt=' + neededFormatCoords + ',pmwtm1~'+ neededFormatCoords;
                return imageSrc;
            })
            .catch(function (error) {
                alert(error.message);
            });
    };

    /**
     * Create new order
     * @param mergedObj {Object} - posted object
     * @returns {Promise}
     */
    this.fetchCreateNewOrder = function (mergedObj) {
        var url = _orderURLTemplate;
        return this.fetchDataPost(url, mergedObj);
    };

    /**
     * Delete order
     * @param orderId {number | string}- order id
     * @returns {Promise}
     */
    this.deleteOrder = function (orderId) {
        var url = _orderURLTemplate + orderId;
        return this.fetchDataDelete(url);
    };


    /**
     * Add product
     * @param orderId {number} - order id
     * @param product {Object} - object product
     * @returns {Promise}
     */
    this.addProduct = function (orderId, product) {
        var url = _orderURLTemplate + orderId + '/products';
        return this.fetchDataPost(url, product);
    };

    /**
     * Get products by order id from server
     * @param orderId {number} - order id
     * @returns {Promise}
     */
    this.getProduct = function (orderId) {
        return new Promise(function (resolve, reject) {
            var req = new XMLHttpRequest();
            var url = 'http://localhost:3000/api/Orders/' + orderId + '/products';
            req.open("GET", url, true);
            req.addEventListener("load", function () {
                if (req.status < 400) {
                    _products = JSON.parse(req.responseText);
                    resolve(JSON.parse(req.responseText));
                } else {
                    reject(new Error("Request failed: " + req.statusText));
                }
            });
            req.addEventListener("error", function () {
                reject(new Error("Network error"));
            });
            req.send();
        });
    };

    /**
     * Delete product from order
     * @param orderId {number} - order id
     * @param productId - product id
     * @returns {Promise}
     */
    this.deleteProduct = function (orderId, productId) {
        var url = _orderURLTemplate + orderId + '/products/' + productId;
        return this.fetchDataDelete(url);
    };


    /**
     * Sort product by asc or desc or default state
     * @param orderId {number} - order id
     * @param sortvalue - sort value
     * @param typeSort - type of sort
     * @returns {Promise}
     */
    this.fetchOrderProduct = function (orderId, sortvalue, typeSort) {
        return new Promise(function (resolve, reject) {
            var req = new XMLHttpRequest();
            var url = 'http://localhost:3000/api/Orders/' + orderId + '/products?filter[order]=' + sortvalue + ' ' + typeSort;
            req.open("GET", url, true);
            req.addEventListener("load", function () {
                if (req.status < 400) {
                    _products = JSON.parse(req.responseText);
                    resolve(JSON.parse(req.responseText));
                } else {
                    reject(new Error("Request failed: " + req.statusText));
                }
            });
            req.addEventListener("error", function () {
                reject(new Error("Network error"));
            });
            req.send();
        });
    };

}