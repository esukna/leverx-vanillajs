/**
 *  Controller application
 * @param view - view application
 * @param model - model application
 * @constructor
 */
function Controller(view, model) {
    /**
     * Current order
     * @type {Object[]};
     */
    var _currentOrder;
    /**
     *  Main this in controller
     * @type {Controller}
     */
    var mainThis = this;

    /**
     * Init fucnction. Init all methods insight.
     */
    this.init = function () {

        /**
         * add Listener to Tab button
         */
        var tabsButton = view.getTabButton();
        tabsButton.addEventListener('click', this._changeTab);
        /**
         * Add listener to list order
         */
        var activeOrder = view.getOrderListElement();
        activeOrder.addEventListener('click', this._changeActiveOrder);
        /**
         * Add listener on order search button
         */
        var searchButton = view.getInputSearchOrdersButtons();
        searchButton.addEventListener('submit', this._seacrhQueryData);
        /**
         * Add listener on order input refresh button
         */
        var refreshButton = view.getInputRefreshButtons();
        refreshButton.addEventListener('click', this._seacrhQueryData);
        /**
         * Add listener on add order button button
         */
        var addOrderBtn = view.getModalAddOrder();
        addOrderBtn.addEventListener('click', this._modalForAddOrder);
        /**
         * Add listener on submit form button
         */
        var submitFormBtn = view.getSubmitFormBtn();
        submitFormBtn.addEventListener('submit', this._submitBtnForm);
        /**
         * Add listener on delete order button
         */
        var clickDeleteOrder = view.getDeleteOrderBtn();
        clickDeleteOrder.addEventListener('click', this._clickDeleteOrder);
        /**
         * Add listener on add product button
         */
        var addProductBtn = view.getAddProduct();
        addProductBtn.addEventListener('click', this._clickAddProduct);
        /**
         * Add listener on delete product button
         */
        var delProductBtn = view.getDelProductBtn();
        delProductBtn.addEventListener('click', this._delProductButton);
        /**
         * Add listener on search table input
         */
        var searchTableInput = view.getSeacrhTableInput();
        searchTableInput.addEventListener('submit', this._searchTableInputCntrl);
        /**
         * Add listener on table tr for sort
         */
        var productTableLabel = view.getgetProductLabel();
        productTableLabel.addEventListener('click', this._orderTableLabel);

        this._setLoadData();
    };

    /**
     * Load data and set first order active. Set data for order info, table info and tabs
     * @private
     */
    this._setLoadData = function () {
        var that = this;
        model
            .fetchOrders()
            .then(function (orderData) {
                view.setOrders(orderData);
                view.setActiveOrder();
                model.setCurrentOrder();
                _currentOrder = model.getCurrentOrder();
                view.setOrderInfo(_currentOrder);
                view.setTabsInfo(_currentOrder);

                var id = model.getCurrentOrder().id;
                model.getProduct(id).then(function (products) {
                    view.setTableInfo(products);
                    var total = products.map(function (value) {
                        return value.totalPrice;
                    }).reduce(function (previousValue, currentValue) {
                        return Number(previousValue) + Number(currentValue);
                    });
                    view.setTotalPrice(total);
                });

                var toggleEdit = view.getEditButton();
                toggleEdit.addEventListener('click', mainThis._toggleEdit);

                that.setCountOrderCntrl();

                return orderData
            })
            .then(function () {
                var coord = _currentOrder.shipTo.address;
                var imageSrc = model.getCodeMapYnadex(coord);
                return imageSrc;
            })
            .then(function (imageSrc) {
                view.setTabsInfo(_currentOrder, imageSrc);
            })
            .catch(function (error) {
                alert(error.message);
            });
    };

    /**
     * Set all info when change order in list order
     * @param event - click on order
     * @private
     */
    this._changeActiveOrder = function (event) {
        var clickedId = view.changeActiveOrder(event);

        view.setActiveOrder(clickedId);
        model.setCurrentOrder(clickedId);
        _currentOrder = model.getCurrentOrder();
        view.setOrderInfo(_currentOrder);

        if(_currentOrder){
            view.setTabsInfo(_currentOrder);
        }

        var id = model.getCurrentOrder().id;
        model.getProduct(id)
            .then(function () {
            mainThis.setCommonTableInfo();
            })
            .catch(function (error) {
                alert(error.message);
            });

        var coord = _currentOrder.shipTo.address;
        model.getCodeMapYnadex(coord)
            .then(function (imageSrc) {
                _currentOrder = model.getCurrentOrder();
                view.setTabsInfo(_currentOrder, imageSrc);
            })
            .catch(function (error) {
                alert(error.message);
            });
    };

    /**
     * Set all info when when change tab
     * @param event - click on tab
     * @private
     */
    this._changeTab = function (event) {
        var changeActiveTab = view.changeActiveTab(event);

        var coord = _currentOrder.shipTo.address;
        model.getCodeMapYnadex(coord)
            .then(function (imageSrc) {
                _currentOrder = model.getCurrentOrder();
                view.setTabsInfo(_currentOrder, imageSrc);
                mainThis.setCommonTableInfo();
            })
            .catch(function (error) {
                alert(error.message);
            });
    };

    /**
     * Set count of Order
     */
    this.setCountOrderCntrl = function () {
        var count = model.getOrders().length;
        view.setCountOrder(count);
    };

    /**
     * Set all info when order search
     * @param event - submit or click in input
     * @private
     */
    this._seacrhQueryData = function (event) {
        var that = mainThis;
        var target = event.target;
        var inputValue = view.getInputSearchValue(target);

        if (event.type == 'submit') {
            that.orderSearchCommonMethods(inputValue);
        } else if (event.type == 'click') {
            that.orderSearchCommonMethods();
        }
        event.preventDefault();
    };

    /**
     * Set active order and info about selected order
     * @param inputValue - value of order input
     */
    this.orderSearchCommonMethods = function (inputValue) {
        var that = this;
        var setValue;
        if(inputValue === undefined){
            setValue = undefined;
        } else {
            setValue = inputValue;
        }

        model
            .fetchOrdersSearch(setValue)
            .then(function (orderData) {
                view.setOrders(orderData);
                that.setCountOrderCntrl();

                if (orderData.length > 0) {
                    view.setActiveOrder();
                    model.setCurrentOrder();
                    view.setTabsInfo(model.getCurrentOrder());
                } else {
                    model.setCurrentOrder();
                }

                _currentOrder = model.getCurrentOrder();
                view.setOrderInfo(_currentOrder);

                var refreshLink = view.refreshListItemLink();
                if(refreshLink){
                    refreshLink.addEventListener('click', that._seacrhQueryData);
                }
                if(model.getCurrentOrder() != null){
                    var id = model.getCurrentOrder().id;
                    model.getProduct(id)
                        .then(function () {
                            mainThis.setCommonTableInfo();
                        })
                        .catch(function (error) {
                            alert(error.message);
                        });
                }
            })
            .catch(function (error) {
                alert(error.message);
                console.log(error);
            });

    };

    /**
     * Toggle edit to save/cancel and if save clicked load on server
     * @param event - click on edit / save cancel button
     * @private
     */
    this._toggleEdit = function (event) {
        var objectSave = view.findEditClickedElement(event);

        if(objectSave) {
            _currentOrder = model.getCurrentOrder();
            model.fetchSave(objectSave, _currentOrder.id);
        }
    };
    /**
     * Call modal for order and set fields inside
     * @private
     */
    this._modalForAddOrder = function () {
        var fieldsInfo = view.modalWindow("form", "order");
        model.setorderFieldsInfo(fieldsInfo);
    };

    /**
     * When submit in modal validate window and get request to add new product or add new order
     * @param event - submit on modal form
     * @private
     */
    this._submitBtnForm = function (event) {
        _currentOrder = model.getCurrentOrder();
        var fieldsInfo = model.getorderFieldsInfo();
        var isProduct = view.isTypeProduct(event);
        var validate  = view.validationModalForm();

        if(isProduct) {
            if(!validate){
                var merged = view.mergeDefaultAndSetData(fieldsInfo, "product", event);
                var id = model.getCurrentOrder().id;
                model.addProduct(id, merged)
                    .then(function () {
                        return model.getProduct(id);
                    })
                    .then(function (value) {
                        view.setTableInfo(value);

                        var products = model.getProducts();

                        mainThis.setCommonTableInfo();
                        view.showOrHideModal();
                    })
                    .catch(function (error) {
                        alert(error.message);
                    });
            }
        } else {
            if(!validate){
                var merged = view.mergeDefaultAndSetData(fieldsInfo, "order", event);
                model.fetchCreateNewOrder(merged)
                    .then(function () {
                        return model.fetchOrders();
                    })
                    .then(function (value) {
                        view.setOrders(value);

                        mainThis.setCountOrderCntrl();
                    })
                    .catch(function (error) {
                        alert(error.message);
                    });
                view.showOrHideModal();
            }
        }
        event.preventDefault();
    };

    /**
     * Delete order and set new info in order info
     * @private
     */
    this._clickDeleteOrder = function () {
        var isAdmin = confirm("Do you want to delete this order?");

        if (isAdmin) {
            var id = model.getCurrentOrder().id;
            model.deleteOrder(id)
                .then(function () {
                    mainThis.setCountOrderCntrl();
                })
                .then(function () {
                    var indexToRemove = model.getOrders().findIndex(function (obj) {
                    return obj.id == id
                })
                .catch(function (error) {
                    alert(error.message);
                });
                model.getOrders().splice(indexToRemove , 1);
                model.setOrders(model.getOrders());
                view.setOrders(model.getOrders());
                view.setActiveOrder();
                model.setCurrentOrder();
                _currentOrder = model.getCurrentOrder();
                view.setOrderInfo(_currentOrder);
                mainThis.setCountOrderCntrl();
            })
        } else {
            console.log('Deletion canceled by user!');
        }
    };

    /**
     * When click on add product call window and set field inside
     * @private
     */
    this._clickAddProduct = function () {
        var fieldsInfo = view.modalWindow("form", "product");
        model.setorderFieldsInfo(fieldsInfo);
    };

    /**
     * Delete product when click on delete product button
     * @param event - Click on delete product button
     * @private
     */
    this._delProductButton = function (event) {
        var productId = view.clickDelBtnProduct(event);

        if(productId){
            var isAdmin = confirm("Do you want to delete this product?");
            if (isAdmin) {
                var orderId = model.getCurrentOrder().id;
                model.deleteProduct(orderId, productId)
                    .then(function () {
                        var products = model.getProducts();

                        var indexToRemove = products.findIndex(function (obj) {
                            return obj.id == productId
                        });
                        products.splice(indexToRemove , 1);

                        mainThis.setCommonTableInfo();
                    })
                    .catch(function (error) {
                        alert(error.message);
                    });
            } else {
                console.log('Deletion canceled by user!');
            }
        }

    };

    /**
     * Collection methods for table info
     */
    this.setCommonTableInfo = function () {
        var products = model.getProducts();
        view.setTableInfo(products);
        if(products.length > 0){
            var total = products.map(function (value) {
                return value.totalPrice;
            }).reduce(function (previousValue, currentValue) {
                return Number(previousValue) + Number(currentValue);
            });
        }
        if(total){
            view.setTotalPrice(total);
        } else  {
            view.setTotalPrice('No Products');
        }
    };

    /**
     * Search product in product table
     * @param event - when submit in search product form
     * @private
     */
    this._searchTableInputCntrl = function (event) {
        view.searchTable(event.target);
        event.preventDefault();
    };

    /**
     * Sort product table
     * @param event - click on tr in thead in product table
     * @private
     */
    this._orderTableLabel = function (event) {
        var nameOfSort = view.clickedTableLabel(event).type;
        var currentSide = view.clickedTableLabel(event).side;
        _currentOrder = model.getCurrentOrder();

        model.fetchProductSort(_currentOrder.id, nameOfSort, currentSide)
            .then(function () {
                mainThis.setCommonTableInfo();
            })
            .catch(function (error) {
                alert(error.message);
            });
    };
}

(
    new Controller(new View(), new Model())
).init();